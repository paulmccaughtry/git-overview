# GIT Overview

This project assumes you are running Linux or Mac. For more information, checkout https://git-scm.com/book/en/v2.

Any items listed between `<text>` are user-supplied values. For example `git branch <branch name>` expects the user to replace the bracket group with the branch name they choose, such as `git branch test-branch`.

## Verify git is installed and install if not
Mac/Linux : `which git`

If no path displayed (e.g., **/usr/bin/git**), install:

- Mac: `brew update && brew install git`
- Linux (Debian flavors): `apt update && apt install -y git`
- Linux (CentOS, etc): `yum update && yum install -y git`

Create global configurations for name, email, editor (https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

## Create a free Bitbucket account
Open an account at https://bitbucket.org.

## Set up SSH Communication with Bitbucket (optional)
Why create ssh keys? Creating ssh keys will allow you to use the git protocol vs https protocol for repository interaction. The main benefit
is lack of supplying credentials to bitbucket if you use https. This becomes cumbersome and ssh allows for convenience.

1. Create ssh keys for your machine (if they do not exist)

    - `ssh-keygen -t rsa -b 4096 -C "<your.email>@<yourdomain.com>"`
    - Verify keys were generated `ls -l ~/.ssh/id_rsa*` (you should see two files, id_rsa (private key) and id_rsa.pub (public key)
    - Additional information: https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html

2. Upload Keys to Bitbucket

    - https://confluence.atlassian.com/bitbucket/use-access-keys-294486051.html

## Clone the Project
Select a parent directory to house the project. **Note**, cloning will build the directory structure for the project so no need to create a containing directory.

    # creates the git-overview directory in the current working directory
	`git clone git@bitbucket.org:paulmccaughtry/git-overview.git`

## Go to the cloned directory 
To update a repository, you must work from the cloned repository/directory to continue the tutorial. Git commands work only in directories initialized as git repositories (have .git directories).

    # go to the cloned directory to continue the tutorial
    `cd git-overview`
    
## Create a local branch
Create a local branch for your work

    `git checkout -b <branch name>`

## Add a file
Create a new file to be included in your branch.

- Utilize `git add <file name>` command to mark the file for inclusion in version control as well as add to workspace.

## Commit changes to a branch in your local repository
Once you make edits to test-file.txt and add a file, commit changes to the local repository and view repository history.

- The `git commit` command will bring up your text editor or use `git commit -m` to write a commit comment inline, then save changes.
- Utilize `git log` to see your commit history.

When you commit changes, files are only commited on your version of the repository, locally. To have your changes available for everyone with access
to the repository you must push your changes to the remote server, usually named "origin". The next section will cover pushing changes.

## Push changes from your local repository to the remote repository
Pushing your changing to a remote repository of the same name provides several opportunities:

 - Collaboration: others can checkout your branch and push to it as well
 - Pull requests: creating a remote branch allows you to create a pull request that includes a code review and merge
    with master branch

    - Utilize `git push -u origin` from the branch you are pushing

## Verify remote branch and create pull request
To verify if your branch is remote, utilize the `git branch -r` command (-r is remote, -a is all)

To create a pull request, push your changes to remote directory and note the pull request link, prefixed with "remote:"
as in the example below (remote:   https://bitbucket.org/paulmccaughtry/git-overview/pull-requests/213?t=1)

Example output from a push command (this is for reference only, do not utilize link):

```
    Counting objects: 12, done.
    Delta compression using up to 8 threads.
    Compressing objects: 100% (10/10), done.
    Writing objects: 100% (12/12), 1.00 KiB | 0 bytes/s, done.
    Total 12 (delta 9), reused 0 (delta 0)
    remote:
    remote: View pull request for uis-713 => master:
    remote:   https://bitbucket.org/paulmccaughtry/git-overview/pull-requests/213?t=1
    remote:
    To bitbucket.org:paulmccaughtry/git-overview.git
       0801f4d2..b50d810a  uis-713 -> uis-713
```

**NOTE**, for the purposes of this tutorial, **DO NOT** save the pull request. The instructions below are information only.

 - Copy the link and paste in browser
 - Select correct branch for merge in drop-down menu
 - Select team member to review
 - Enable checkbox for close remote branch after merge
 - After merge, delete local branch

For this tutorial, **DO**:

 - Verify the bitbucket pull request appears to be populated correctly
 - Close browser window without saving pull request
 - Delete remote branch with `git branch` command from documentation
 - Delete local branch with `git branch` command from documentation
 - Delete directory structure from machine if no longer wanted

## Extra credit
The following items will help you become familiar with git.

 - Edit the existing `./test-file.txt` based on the instructions in the file.
 - Set up additional global configs for your git account (https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)
 - Create git aliases to reduce key strokes
 - If coming from SVN, read, https://git-scm.com/course/svn.html
 - Learn about git-ignore file
